CREATE TABLE `mylibray`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(15) NOT NULL,
  `Username` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(15) NOT NULL,
  `Gender` VARCHAR(1) NOT NULL,
  `Address` VARCHAR(45) NOT NULL,
  `City` VARCHAR(15) NOT NULL,
  `State` VARCHAR(15) NOT NULL,
  `Idproof` VARCHAR(15) NULL,
  `Idphoto` BLOB NULL,
  `Createtimestamp` TIMESTAMP(6) NULL,
  UNIQUE INDEX `Username_UNIQUE` (`Username` ASC) VISIBLE,
  PRIMARY KEY (`id`));